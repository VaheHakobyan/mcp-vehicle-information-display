################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../tm4c1294ncpdt.cmd 

C_SRCS += \
../display.c \
../main.c \
../sin1.c \
../tm4c1294ncpdt_startup_ccs.c 

C_DEPS += \
./display.d \
./main.d \
./sin1.d \
./tm4c1294ncpdt_startup_ccs.d 

OBJS += \
./display.obj \
./main.obj \
./sin1.obj \
./tm4c1294ncpdt_startup_ccs.obj 

OBJS__QUOTED += \
"display.obj" \
"main.obj" \
"sin1.obj" \
"tm4c1294ncpdt_startup_ccs.obj" 

C_DEPS__QUOTED += \
"display.d" \
"main.d" \
"sin1.d" \
"tm4c1294ncpdt_startup_ccs.d" 

C_SRCS__QUOTED += \
"../display.c" \
"../main.c" \
"../sin1.c" \
"../tm4c1294ncpdt_startup_ccs.c" 


