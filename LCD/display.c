// Program for LCD Display 800x480 RGB to display a tachometer and trip odometer display
// Display controller Type SSD 1963 => Solomon Systech
// Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
// V0.1-V0.3 V. Hakobyan 22.11.2020

#include <stdint.h>
#include <stdbool.h>                    // type bool for giop.h
#include "inc/hw_types.h"
#include "inc/tm4c1294ncpdt.h"
#include <stdio.h>                      // Debug only
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>             // GPIO_PIN_X
#include <inc/hw_memmap.h>              // GPIO_PORTX_BASE
#include "sin1.h"
#include <time.h>
#include "display.h"

/********************************************************************************
     Elementary output functions  => speed optimized as inline
*********************************************************************************/
inline void write_command(unsigned char command)
{
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * Write command byte to LC Display
     * input parameters:
     *    unsigned char command = the command byte to write on LCD
     *---------------------------------------------------------------
     */
    GPIO_PORTM_DATA_R = command;        // Write command byte
    GPIO_PORTL_DATA_R = 0x11;           // Chip select = 0, Command mode select = 0, Write state = 0
    GPIO_PORTL_DATA_R = 0x1F;           // Initial state
}
/********************************************************************************/
inline void write_data(unsigned char data)
{
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * Write data byte to LC Display
     * input parameters:
     *    unsigned char data = the data byte to write on LCD
     *---------------------------------------------------------------
     */
    GPIO_PORTM_DATA_R = data;           // Write data byte
    GPIO_PORTL_DATA_R = 0x15;           // Chip select = 0, Write state = 0
    GPIO_PORTL_DATA_R = 0x1F;           // Initial state
}
/********************************************************************************/
inline void init_ports_display(void)
{
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * Initializing the ports of the Display
     *---------------------------------------------------------------
     */
    // Set Port M Pins 0-7: used as Output of LCD Data
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOM);            // enable clock-gate Port M
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOM));     // wait until clock ready
    GPIOPinTypeGPIOOutput(GPIO_PORTM_BASE, 0xFF);
    // Set Port L Pins 0-4: used as Output of LCD Control signals:
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);            // Clock Port Q
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOL));
    GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3| GPIO_PIN_4);
}
/********************************************************************************/
void configure_display_controller_large (void) // 800 x 480 pixel ???
{
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * Configuring the Display
     *---------------------------------------------------------------
     */
    GPIO_PORTL_DATA_R = INITIAL_STATE;      // Initial state
    GPIO_PORTL_DATA_R &= ~RST;              // Hardware reset
    SysCtlDelay(10000);                     // wait >1 ms
    GPIO_PORTL_DATA_R |= RST;
    SysCtlDelay(12000);                     // wait >1 ms

    write_command(SOFTWARE_RESET);          // Software reset
    SysCtlDelay(120000);                    // wait >10 ms

    write_command(SET_PLL_MN);               // Set PLL Freq to 120 MHz
    write_data(0x24);
    write_data(0x02);
    write_data(0x04);

    write_command(START_PLL);                // Start PLL
    write_data(0x01);
    SysCtlDelay(10000);                      // wait 1 ms

    write_command(START_PLL);                // Lock PLL
    write_data(0x03);
    SysCtlDelay(10000);                      // wait 1 ms

    write_command(SOFTWARE_RESET);           // Software reset
    SysCtlDelay(100000);                     // wait 10 ms

    write_command(0xe6);                    // Set pixel clock frequency
    write_data(0x01);                       // KRR Set LCD Pixel Clock 9MHz
    write_data(0x70);                       // KRR
    write_data(0xA3);                       // KRR

    write_command(SET_LCD_MODE);          // SET LCD MODE SIZE, manual p. 44
    write_data(0x20);                     // ..TFT panel 24bit
    write_data(0x00);                     // ..TFT mode
    write_data(0x03);                     // SET horizontal size = 800-1 (high byte)
    write_data(0x1F);                     // SET horizontal size = 800-1 (low byte)
    write_data(0x01);                     // Set vertical size = 480-1 (high byte)
    write_data(0xDF);                     // Set vertical size = 480-1 (low byte)
    write_data(0x00);                     // Even line RGB sequence / Odd line RGB sequence RGB

    write_command(SET_HORI_PERIOD);       // Set Horizontal Period
    write_data(0x03);                     // Horizontal total period (display + non-displayed)  (high byte)
    write_data(0x5E);                     // Horizontal total period (display + non-display) (low byte)
    write_data(0x00);                     // Non-displayed period between the start of the horizontal sync (LLINE) signal and the first displayed data.
    write_data(0x46);                     // Low byte of the non-display period between the start of the horizontal sync (LLINE) signal and the first display data
    write_data(0x09);                     // Set the vertical sync width
    write_data(0x00);                     // Set horiz.Sync pulse start    (high byte)
    write_data(0x08);                     // Set horiz.Sync pulse start    (low byte)
    write_data(0x00);


    write_command(SET_VERT_PERIOD);         // Set vertical periods, manual  p. 49
    write_data(0x01);                       // Vertical total period (display + non-displayed) in lines (high byte)
    write_data(0xFE);                       // as above (low byte) = total 510  lines
    write_data(0x00);
    write_data(0x0C);                       // The non-displayed period in lines between the start of the frame and the first
                                            // display data = 12 line.s
    write_data(0x0A);                       // Set the vertiacla sync width = 10 pixels
    write_data(0x00);                       // Set vertical sync pulse start position (high byte)
    write_data(0x04);                       // as above (low byte) = total sync pulse start position is 4 lines

    write_command(SET_ADRESS_MODE);         // Pixel address counting = flip display , manual p. 36
    write_data(0x03);                       // necessary to match with touch screen addressing

//  write_command(0x0A);                    // Power control mode not tested in detail
//  write_data(0x1C);

    write_command(SET_PIXEL_DATA_FORMAT);    // set pixel data format 8bit manual p. 78
    write_data(0x00);

    write_command(SET_DISPLAY_ON);           // Set display on  manual p. 78
}
/********************************************************************************/
inline void configPorts(void)
    {
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * Configuration of ports P and L for the signals s1 ans s2 and the touch display
     *---------------------------------------------------------------
     */
        // enable ports
        SYSCTL_RCGCGPIO_R |= 0x3008; // port P and L
        // wait for the cores to power up
        while((SYSCTL_PRGPIO_R & (0x3008)) == 0);

        GPIO_PORTN_DEN_R        |= 0x03;    // Digital Enable for PN0 and PN1
        GPIO_PORTN_DIR_R        |= 0x03;    // Define port as Output for PIN PN0 and PN1
        GPIO_PORTP_DEN_R        |= 0x03;    // Digital Enable for PP0 and PP1
        GPIO_PORTP_DIR_R        &= 0xFC;    // Define port as Input for PIN PP0 and PP1
        GPIO_PORTD_AHB_DEN_R = 0x1F;            //PortD digital enable
        GPIO_PORTD_AHB_DIR_R = 0x0D;            //PortD Input/Output
        GPIO_PORTD_AHB_DATA_R &= 0xF7;          //Clk=0
    }
/********************************************************************************/
inline void print_window(int min_x,int min_y,int max_x,int max_y, enum colors color)
{
    /*--------------------------------------------------------------
     * Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
     * printing a rectangle on the LCD
     * input parameters:
     *    int min_x, min_y = Coordinates of the starting point
     *    int max_x, max_y = Coordinates of the ending point
     *    length = max_x-min_x and height = max_y-min_y
     *    enum colors color = the color of the window
     *---------------------------------------------------------------
     */
    int x,y;
    write_command(0x2A);           // Set row address x-axis
    write_data(min_x >> 8);        // Set start  address           (high byte)
    write_data(min_x);             // as above                     (low byte)
    write_data(max_x >> 8);        // Set stop address             (high byte)
    write_data(max_x);             // as above                     (low byte)

    write_command(0x2B);           // Set column address (y-axis)
    write_data(min_y >> 8);        // Set start column address     (high byte)
    write_data(min_y);             // as above                     (low byte)
    write_data(max_y >> 8);        // Set stop column address      (high byte)
    write_data(max_y);             // as above                     (low byte)

    write_command(0x2C); //write pixel command
    for (x=min_x;x<=max_x;x++)
        for (y=min_y;y<=max_y;y++)
        {
            write_data((color>>16)&0xff); // red
            write_data((color>>8)&0xff); // green
            write_data((color)&0xff); // blue
        }
}
/********************************************************************************/
inline void print_pixel(int x, int y, enum colors color)
{
    /*--------------------------------------------------------------
     * Printing a pixel on the LCD
     * input parameters:
     *    int x, y = Coordinates of the pixel
     *    enum colors color = the color of the pixel
     *---------------------------------------------------------------
     */
    write_command(0x2A);       // Set row address x-axis
    write_data(x >> 8);        // Set start  address           (high byte)
    write_data(x);             // as above                     (low byte)
    write_data(x >> 8);        // Set start  address           (high byte)
    write_data(x);             // as above                     (low byte)

    write_command(0x2B);       // Set column address (y-axis)
    write_data(y >> 8);        // Set start column address     (high byte)
    write_data(y);             // as above                     (low byte)
    write_data(y >> 8);        // Set start column address     (high byte)
    write_data(y);             // as above                     (low byte)

    write_command(0x2C);            //write pixel command
    write_data((color>>16)&0xff);   // red
    write_data((color>>8)&0xff);    // green
    write_data((color)&0xff);       // blue
}
/********************************************************************************/
inline void print_point(int x, int y, int size, enum colors color)
{
    /*--------------------------------------------------------------
     * Printing a point on the LCD
     * input parameters:
     *    int x, y = Coordinates of the point
     *    int size = Size of the point
     *    enum colors color = the color of the point
     * you can make characters and lines thicker by setting size bigger than 1
     *---------------------------------------------------------------
     */
    int i,j;
    write_command(0x2A);           // Set row address x-axis
    write_data((x-size) >> 8);     // Set start  address           (high byte)
    write_data(x-size);            // as above                     (low byte)
    write_data((x+size) >> 8);     // Set start  address           (high byte)
    write_data(x+size);            // as above                     (low byte)

    write_command(0x2B);           // Set column address (y-axis)
    write_data((y-size) >> 8);     // Set start column address     (high byte)
    write_data(y-size);            // as above                     (low byte)
    write_data((y+size) >> 8);     // Set start column address     (high byte)
    write_data(y+size);            // as above                     (low byte)

    write_command(0x2C); //write pixel command
    for (i=(x-size);i<=(x+size);i++)
        for (j=(y-size);j<=(y+size);j++)
        {
            write_data((color>>16)&0xff);  // red
            write_data((color>>8)&0xff);   // green
            write_data((color)&0xff);      // blue
        }
}
/********************************************************************************/
inline void clear_window(int min_x, int min_y, int max_x, int max_y)
{
    /*--------------------------------------------------------------
     * Clearing a already printed window setting the pixels black
     * input parameters:
     *    int min_x, min_y = Coordinates of the starting point
     *    int max_x, max_y = Coordinates of the ending point
     *    length = max_x-min_x and height = max_y-min_y
     *    clear a area setting the pixels of window in black
     *---------------------------------------------------------------
     */
    print_window(min_x,min_y,max_x,max_y,BLACK);
}
/********************************************************************************/
inline void clear_whole_background()
{
    /*--------------------------------------------------------------
     * input parameters:
     *    enum colors color =
     *    set the whole display in black and "deleting" all the stuff
     *---------------------------------------------------------------
     */
    print_window(0,0,MAX_X-1,MAX_Y-1,BLACK);
}
/********************************************************************************/
inline void touch_write(unsigned char value)
{
    /*--------------------------------------------------------------
     * Source: K.R. Riemschneider - Testprogram für Touch-Display TEST_TOUCH.c
     * Writing the touch command
     * input parameters:
     *    unsigned char value = which command for touch (0xD0 -> XPos read)
     *                                                  (0x90 -> YPos read)
     *---------------------------------------------------------------
     */
    unsigned char i = 0x08; // 8 bit command
    unsigned char DI;
    GPIO_PORTD_AHB_DATA_R &= 0xFB; //CS=0
    while (i > 0) {
        DI = (value >> 7);
        if (DI == 0) {GPIO_PORTD_AHB_DATA_R &= 0xfe;} //out bit=0
        else {GPIO_PORTD_AHB_DATA_R |= 0x01;} //out bit=1
        value <<= 1; //next value
        GPIO_PORTD_AHB_DATA_R |= 0x08; //Clk=1
        GPIO_PORTD_AHB_DATA_R &= 0xf7; //Clk=0
        i--;
    }
}
/********************************************************************************/
inline unsigned int touch_read()
{
    /*--------------------------------------------------------------
     * Source: K.R. Riemschneider - Testprogram für Touch-Display TEST_TOUCH.c
     * Reading a coordinate of position of a pixel
     *---------------------------------------------------------------
     */
    unsigned char i = 12; // 12 Bit ADC
    unsigned int value = 0x00;
    while (i > 0)
    {
        value <<= 1;
        GPIO_PORTD_AHB_DATA_R |= 0x08; //Clk=1
        GPIO_PORTD_AHB_DATA_R &= 0xf7; //Clk=0
        value |= ((GPIO_PORTD_AHB_DATA_R >> 1) & 0x01); // read value
        i--;
    }
    GPIO_PORTD_AHB_DATA_R |= 0x04; //CS=1
    return value;
}
inline int sgn(int x){
    /*--------------------------------------------------------------
     * Signum function for the function set_line
     * Assigning the sign of the number
     *  input parameters:
     *        int x = the number to test
     *---------------------------------------------------------------
     */
    return (x > 0) ? 1 : (x < 0) ? -1 : 0;
}
/********************************************************************************/
inline void print_line(double xstart,double ystart,double xend,double yend, enum colors color)
{
    /*--------------------------------------------------------------
     * Source: Bresenham-Algorithmus
     * Draw lines on raster devices
     *  input parameters:
     *        int xstart, ystart = coordinates of the starting point
     *        int xend, yend = coordinates of the ending point
     *---------------------------------------------------------------
     */
    double x, y, t, dx, dy, incx, incy, pdx, pdy, ddx, ddy, deltaslowdirection, deltafastdirection, err;

    /* Entfernung in beiden Dimensionen berechnen */
      dx = xend - xstart;
      dy = yend - ystart;

    /* Vorzeichen des Inkrements bestimmen */
      incx = sgn(dx);
      incy = sgn(dy);
      if(dx<0) dx = -dx;
      if(dy<0) dy = -dy;

    /* feststellen, welche Entfernung größer ist */
      if (dx>dy)
      {
         /* x ist schnelle Richtung */
         pdx=incx; pdy=0;    /* pd. ist Parallelschritt */
         ddx=incx; ddy=incy; /* dd. ist Diagonalschritt */
         deltaslowdirection =dy;   deltafastdirection =dx;   /* Delta in langsamer Richtung, Delta in schneller Richtung */
      } else
      {
         /* y ist schnelle Richtung */
         pdx=0;    pdy=incy; /* pd. ist Parallelschritt */
         ddx=incx; ddy=incy; /* dd. ist Diagonalschritt */
         deltaslowdirection =dx;   deltafastdirection =dy;   /* Delta in langsamer Richtung, Delta in schneller Richtung */
      }

    /* Initialisierungen vor Schleifenbeginn */
      x = xstart;
      y = ystart;
      err = deltafastdirection/2;
      print_pixel(x,y,color);

    /* Pixel berechnen */
      for(t=0; t<deltafastdirection; ++t) /* t zaehlt die Pixel, deltafastdirection ist Anzahl der Schritte */
      {
         /* Aktualisierung Fehlerterm */
         err -= deltaslowdirection;
         if(err<0)
         {
             /* Fehlerterm wieder positiv (>=0) machen */
             err += deltafastdirection;
             /* Schritt in langsame Richtung, Diagonalschritt */
             x += ddx;
             y += ddy;
         } else
         {
             /* Schritt in schnelle Richtung, Parallelschritt */
             x += pdx;
             y += pdy;
         }
         print_pixel(x,y,color);
      }
}
/********************************************************************************/
// this function is more flexible and makes possible to set a line with a given length without losing the orientation
inline void print_flexible_line(double xstart,double ystart,double xend,double yend, double length, enum colors color)
{
    /*--------------------------------------------------------------
     * Source: Bresenham-Algorithmus
     * Draw lines on raster devices
     *  input parameters:
     *        int xstart, ystart = coordinates of the starting point
     *        int xend, yend = coordinates of the orientation point
     *        double length = length of the line
     *---------------------------------------------------------------
     */
    double x, y, t, dx, dy, incx, incy, pdx, pdy, ddx, ddy, deltaslowdirection, deltafastdirection, err;

    /* Entfernung in beiden Dimensionen berechnen */
      dx = xend - xstart;
      dy = yend - ystart;

    /* Vorzeichen des Inkrements bestimmen */
      incx = sgn(dx);
      incy = sgn(dy);
      if(dx<0) dx = -dx;
      if(dy<0) dy = -dy;

    /* feststellen, welche Entfernung größer ist */
      if (dx>dy)
      {
         /* x ist schnelle Richtung */
         pdx=incx; pdy=0;    /* pd. ist Parallelschritt */
         ddx=incx; ddy=incy; /* dd. ist Diagonalschritt */
         deltaslowdirection =dy;   deltafastdirection =dx;   /* Delta in langsamer Richtung, Delta in schneller Richtung */
      } else
      {
         /* y ist schnelle Richtung */
         pdx=0;    pdy=incy; /* pd. ist Parallelschritt */
         ddx=incx; ddy=incy; /* dd. ist Diagonalschritt */
         deltaslowdirection =dx;   deltafastdirection =dy;   /* Delta in langsamer Richtung, Delta in schneller Richtung */
      }

    /* Initialisierungen vor Schleifenbeginn */
      x = xstart;
      y = ystart;
      err = deltafastdirection/2;
      print_pixel(x,y,color);

    /* Pixel berechnen */
      for(t=0; t<deltafastdirection; ++t) /* t zaehlt die Pixel, deltafastdirection ist Anzahl der Schritte */
      {
          if (t==length) // with this statement the line will be canceled after reaching the given length
              break;
         /* Aktualisierung Fehlerterm */
         err -= deltaslowdirection;
         if(err<0)
         {
             /* Fehlerterm wieder positiv (>=0) machen */
             err += deltafastdirection;
             /* Schritt in langsame Richtung, Diagonalschritt */
             x += ddx;
             y += ddy;
         } else
         {
             /* Schritt in schnelle Richtung, Parallelschritt */
             x += pdx;
             y += pdy;
         }
         print_pixel(x,y,color);
      }
}
/********************************************************************************/
inline void print_character(char character, int x, int y, int width, enum colors color)
{
    /*--------------------------------------------------------------
     * Setting a character in a given position
     * input parameters:
     *    char character = the character which has to be printed out
     *    int x, y = Coordinates of the lower right pixel of the character
     *    int width = the width of the character. Height of the character is two times bigger
     *    enum colors color = the color of the character
     * pay attention on the fact that the coordinates are for the lower right pixel and not lower left
     * not all characters are made. You can make new characters if they are needed
     *---------------------------------------------------------------
     */
    double height=2*width;      // calculate the height with the given width (can be changed to e.g. 1.7*height)
    double gap_letter=width/8;  // the gap in the lines by making the letters for making the corners of letters visible
    double gap_digit=width/5;   // the gap in the lines by making the numbers for making the corners of numbers visible
    switch(character)
    {
    case '0':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width,y-width+gap_digit,x-width,y-gap_digit,color);
        print_line(x-width,y-height+gap_digit,x-width,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '1':
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '2':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width,y-width+gap_digit,x-width,y-gap_digit,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        break;
    case '3':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '4':
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x-width,y-height+gap_digit,x-width,y-width-gap_digit,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '5':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x-width,y-height+3,x-width,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '6':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width,y-width+gap_digit,x-width,y-gap_digit,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x-width,y-height+gap_digit,x-width,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '7':
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '8':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width,y-width+gap_digit,x-width,y-gap_digit,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x-width,y-height+gap_digit,x-width,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case '9':
        print_line(x-width+gap_digit,y,x-gap_digit,y,color);
        print_line(x-width+gap_digit,y-width,x-gap_digit,y-width,color);
        print_line(x-width,y-height+gap_digit,x-width,y-width-gap_digit,color);
        print_line(x-width+gap_digit,y-height,x-gap_digit,y-height,color);
        print_line(x,y-height+gap_digit,x,y-width-gap_digit,color);
        print_line(x,y-width+gap_digit,x,y-gap_digit,color);
        break;
    case 'A':
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'B':
        print_line(x-width,y,x-gap_letter,y,color);
        print_line(x-width,y-height,x-width,y,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x-width,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-width-gap_letter,color);
        print_line(x,y-width+gap_letter,x,y-gap_letter,color);
        break;
    case 'C':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        break;
    case 'D':
        print_line(x-width,y,x-gap_letter,y,color);
        print_line(x-width,y-height,x-width,y,color);
        print_line(x-width,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'E':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-2*width,x-gap_letter,y-height,color);
        break;
    case 'F':
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x-width,y-width,x,y-width,color);
        break;
    case 'G':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width/2,y-width,x-gap_letter,y-width,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width,y-height,x-gap_letter,y-height,color);
        print_line(x,y-width+gap_letter,x,y-gap_letter,color);
        break;
    case 'H':
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'I':
        print_line(x-width/2,y-height+gap_letter,x-width/2,y-gap_letter,color);
        break;
    case 'J':
        print_line(x-width,y,x-gap_letter,y,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'K':
        print_line(x-width,y-height,x-width,y,color);
        print_line(x-width,y-width,x,y-height,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x,y-width+gap_letter,x,y-gap_letter,color);
        break;
    case 'L':
        print_line(x-width+gap_letter,y,x,y,color);
        print_line(x-width,y-height,x-width,y-gap_letter,color);
        break;
    case 'M':
        print_line(x-width,y-height+gap_letter,x-width,y,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x-width/2,y-height+gap_letter,x-width/2,y-width,color);
        print_line(x,y-height+gap_letter,x,y,color);
        break;
    case 'N':
        print_line(x-width,y-height+gap_letter,x-width,y,color);
        print_line(x-width,y-height+gap_letter,x-gap_letter,y,color);
        print_line(x,y-height+gap_letter,x,y,color);
        break;
    case 'O':
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'P':
        print_line(x-width,y-height+gap_letter,x-width,y,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-width-gap_letter,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        break;
    case 'Q':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y,color);
        print_line(x-width/4,y-width/4,x+width/4,y+width/4,color);
        break;
    case 'R':
        print_line(x-width,y-height+gap_letter,x-width,y,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-height+gap_letter,x,y-width-gap_letter,color);
        print_line(x-width,y-width,x,y,color);
        print_line(x-width+1,y-width,x-gap_letter,y-width,color);
        break;
    case 'S':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x-width,y-height+gap_letter,x-width,y-width-gap_letter,color);
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x,y-width+gap_letter,x,y-gap_letter,color);
        break;
    case 'T':
        print_line(x-width+gap_letter,y-height,x-gap_letter,y-height,color);
        print_line(x-width/2,y-height+gap_letter,x-width/2,y,color);
        break;
    case 'U':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        break;
    case 'V':
        print_line(x-width,y-height+gap_letter,x-width,y,color);
        print_line(x,y-height,x,y-width,color);
        print_line(x-width,y,x,y-width,color);
        break;
    case 'W':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width,y-height+gap_letter,x-width,y-gap_letter,color);
        print_line(x,y-height+gap_letter,x,y-gap_letter,color);
        print_line(x-width/2,y-width,x-width/2,y-gap_letter,color);
        break;
    case 'X':
        print_line(x-width,y,x,y-height,color);
        print_line(x-width,y-height,x,y,color);
        break;
    case 'Y':
        print_line(x-width+gap_letter,y,x-gap_letter,y,color);
        print_line(x-width+gap_letter,y-width,x-gap_letter,y-width,color);
        print_line(x-width,y-height+gap_letter,x-width,y-width-gap_letter,color);
        print_line(x,y-height+gap_letter,x,y,color);
        break;
    case 'Z':
        print_line(x-width,y,x,y,color);
        print_line(x-width,y,x,y-height,color);
        print_line(x-width,y-height,x,y-height,color);
        break;
    case '/':
        print_line(x-width,y,x,y-height,color);
        break;
    case '\\':
        print_line(x-width,y-height,x,y,color);
        break;
    case '.':
        print_line(x-width/2,y,x-width/2,y+width/4,color);
        break;
    case '!':
        print_line(x-width/2,y-width/2,x-width/2,y-height, color);
        print_line(x-width/2,y,x-width/2,y-1,color);
        break;
    case ':':
        print_line(x-width/2,y-height*5/6,x-width/2,y-height*4/6,color);
        print_line(x-width/2,y-height*2/6,x-width/2,y-height/6,color);
        break;
    case ' ':
        break;
    default:
        break;
    }
}
/********************************************************************************/
inline void print_a_text(char *text, int x, int y, int width, enum colors color)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    char *text = the text as pointer array which has to be printed out
     *    int x, y = Coordinates of the lower right pixel of the first character
     *    int width = the width of the characters. Height of the character is two times bigger
     *    enum colors color = the color of the character
     * function to set a text at a given position
     * pay attention on the fact that the coordinates are for the lower right pixel and not lower left
     * not all characters are made. You can make new characters if they are needed
     *---------------------------------------------------------------
     */
    int i;
    int spacing=width;              // the spacing between the characters in the text
    for (i=0;i<strlen(text);i++)
    {
        print_character(text[i],x+i*width+i*spacing,y,width, color);
    }
}
/********************************************************************************/
inline void print_unfilled_window(int min_x, int min_y, int max_x, int max_y, enum colors color)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    int min_x, min_y = Coordinates of the starting point
     *    int max_x, max_y = Coordinates of the ending point
     *    length = max_x-min_x and height = max_y-min_y
     *    enum colors color = the color of the window
     *    setting a unfilled window as a rectangle
     *---------------------------------------------------------------
     */
    print_line(min_x,min_y,min_x,max_y,color);
    print_line(min_x,max_y,max_x,max_y,color);
    print_line(max_x,max_y,max_x,min_y,color);
    print_line(max_x,min_y,min_x,min_y,color);
}

inline void print_ellipse(int xm, int ym, int a, int b, enum colors color)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    int xm, int = Coordinates of the midpoint of the ellipse
     *    int a, b = the both radiuses of the ellipse
     *    enum colors color = the color of the ellipse
     * setting an ellipse at given midpoint
     * a=b if you need a circle
     *---------------------------------------------------------------
   */
   int dx = 0, dy = b; /* im I. Quadranten von links oben nach rechts unten */
   long a2 = a*a, b2 = b*b;
   long err = b2-(2*b-1)*a2, e2; /* Fehler im 1. Schritt */

   do {
       print_point(xm+dx,ym+dy,1,color); /* I. Quadrant */
       print_point(xm-dx, ym+dy,1, color); /* II. Quadrant */
       print_point(xm-dx, ym-dy,1, color); /* III. Quadrant */
       print_point(xm+dx, ym-dy,1, color); /* IV. Quadrant */
       e2 = 2*err;
       if (e2 <  (2*dx+1)*b2) { dx++; err += (2*dx+1)*b2; }
       if (e2 > -(2*dy-1)*a2) { dy--; err -= (2*dy-1)*a2; }
   } while (dy >= 0);

   while (dx++ < a) { /* fehlerhafter Abbruch bei flachen Ellipsen (b=1) */
       print_point(xm+dx, ym,1, color); /* -> Spitze der Ellipse vollenden */
       print_point(xm-dx, ym,1, color);
   }
}
/********************************************************************************/
void print_immutable_components_of_tacho(int x0,int y0, enum colors color)
{
    /*--------------------------------------------------------------
     * Only printing the components of tachometer which will be called once
     * input parameters:
     *    int x0, y0 = the coordinates of the midpoint of tachometer
     *    enum colors color = the color of the circles and lines in the speedometer
     * changing the radius might cause errors in printing out the characters
     *---------------------------------------------------------------
   */
    int radius = 200;
    int f = 1 - radius;
    int ddF_x = 0;
    int ddF_y = -2 * radius;
    int x = 0;
    int y = radius;

    print_ellipse(x0,y0,radius*7/10,radius*7/10,color);   // the inner circle of the speedometer
    print_ellipse(x0,y0,radius,radius,color);             // the outer circle of the speedometer

    print_flexible_line(x0, y0 - radius,x0,y0,5, color);  // setting the small lines showing at speeds
    print_a_text("200",x0-16,y0-radius+47,8,WHITE);   // printing out the speeds
    print_flexible_line(x0 + radius, y0,x0,y0,5, color);
    print_a_text("340",x0+radius-26,y0+2,4,YELLOW);
    print_flexible_line(x0 - radius, y0,x0,y0,5, color);
    print_a_text("60",x0-radius+16,y0+2,4,YELLOW);
    print_a_text("KM/H",x0-23,y0+radius-23,7,WHITE);
    while(x < y)    // an algorithmus for printing out the small lines and speeds
    {
      if(f >= 0)
      {
        y--;
        ddF_y += 2;
        f += ddF_y;
      }
      x++;
      ddF_x += 2;
      f += ddF_x + 1;
      if (x%40==0)
      {

          print_flexible_line(x0+x,y0-y,x0,y0,5,color);
          print_flexible_line(x0-x,y0-y,x0,y0,5,color);
          print_flexible_line(x0+y,y0+x,x0,y0,5,color);
          print_flexible_line(x0-y,y0+x,x0,y0,5,color);
          print_flexible_line(x0+y,y0-x,x0,y0,5,color);
          print_flexible_line(x0-y,y0-x,x0,y0,5,color);
          if (x==40)
          {
              print_a_text("220",x0+x-8,y0-y+15,4,YELLOW);
              print_a_text("180",x0-x-8,y0-y+15,4,YELLOW);
              print_a_text("360",x0+y-45,y0+x+5,8,WHITE);
              print_a_text("40",x0-y+35,y0+x,8,WHITE);
              print_a_text("320",x0+y-47,y0-x+10,8,WHITE);
              print_a_text("80",x0-y+35,y0-x+10,8,WHITE);
          }
          else if (x==80)
          {
              print_a_text("240",x0+x-25,y0-y+42,8,WHITE);
              print_a_text("160",x0-x,y0-y+42,8,WHITE);
              print_a_text("380",x0+y-23,y0+x,4,YELLOW);
              print_a_text("20",x0-y+15,y0+x,4,YELLOW);
              print_a_text("300",x0+y-25,y0-x+5,4,YELLOW);
              print_a_text("100",x0-y+12,y0-x+6,4,YELLOW);
          }
          else
          {
              print_flexible_line(x0+x,y0+y,x0,y0,radius*3/10-12, color);
              print_flexible_line(x0-x,y0+y,x0,y0,radius*3/10-12, color);
              print_a_text("260",x0+x-15,y0-y+15,4,YELLOW);
              print_a_text("140",x0-x+8,y0-y+15,4,YELLOW);
              print_a_text("400",x0+y-45,y0+x-5,8,WHITE);
              print_a_text("0",x0-y+43,y0+x-12,8,WHITE);
              print_a_text("280",x0+y-45,y0-x+25,8,WHITE);
              print_a_text("120",x0-y+20,y0-x+25,8,WHITE);
          }
      }
    }
}
/********************************************************************************/
inline void print_tacho(double speed,double *speed_old,int x0,int y0)
{
    /*--------------------------------------------------------------
     * Updating the tachometer pointer
     * input parameters:
     *    int speed, speed_old = the new and old speed of the motor
     *    int x0, y0 = the coordinates of the midpoint of tachometer
     *    int radius = the length of the radius of the circle
     *    enum colors color = the color of the circles and lines in the speedometer
     * changing the radius might cause errors in printing out the characters
     *---------------------------------------------------------------
   */
    double x1,y1,grad;
    int length,i;
    length=200*7/10-2;              // the length of the pointer (radius = 200)

    if ((speed>=0) && (speed<=400)) // one km/h corresponds to 0.635 degree
    {
        grad=(*speed_old)*0.635+53;
        *speed_old=speed;
        x1=length*sin1(grad*32768.0/ 360.0)*Q15;
        y1=length*cos1(grad*32768.0/ 360.0)*Q15;
        print_flexible_line(x0-x1,y0+y1,x0,y0,length,BLACK);  // deleting the pointer showing at the old speed
        grad=speed*0.635+53;
        x1=length*sin1(grad*32768.0/ 360.0)*Q15;
        y1=length*cos1(grad*32768.0/ 360.0)*Q15;
        print_flexible_line(x0-x1,y0+y1,x0,y0,length,RED);    // setting the new pointer showing at the new speed
    }
}
/********************************************************************************/
inline void print_immutable_components_of_odometer( int x, int y, int width, enum colors color)
{
    /*--------------------------------------------------------------
     * Only printing the components of odometer which will be called once
     * input parameters:
     *    int x, y = the coordinates of the midpoint of odometer
     *    enum colors color = the color of the circles and lines in the odometer
     *---------------------------------------------------------------
   */
    int height=2*width;
    int spacing=width;
    print_a_text("DISTANCE",x+width/2+4*width, y, width/2, WHITE);
    print_unfilled_window(x,y+spacing,x+9*width+7*spacing,y+spacing+2*height,color);
    print_a_text("KM",x+7*(width+spacing),y+2*height,width/2,WHITE);
}
/********************************************************************************/
inline void print_odometer(double distance, double distance_old, int x, int y, int width)
{
    /*--------------------------------------------------------------
     * Updating the odometer
     * input parameters:
     *    double distance, distance_old = the new and old distances
     *    int x, y = the coordinates of the upper left corner of the distance display
     *    int widht = the width of the characters of the text in description
     *    enum colors color = the color of the window
     *---------------------------------------------------------------
   */
    int height=2*width;
    int spacing=width;
    char str_distance[7];
    char str_distance_old[7];
    snprintf(str_distance,7,"%.2f", distance);          // convert double in string
    snprintf(str_distance_old,7,"%.2f", distance_old);
    if (distance_old != distance)                        // deleting the old distance if it is changed
        print_a_text(str_distance_old,x+spacing+width,y+2*height,width, BLACK);
    print_a_text(str_distance,x+spacing+width,y+2*height,width, WHITE); // setting the new distance
}
/********************************************************************************/
inline void print_immutable_components_of_move_dir(int x, int y, int width)
{
    /*--------------------------------------------------------------
     * Only printing the components of moving direction display which will be called once
     * input parameters:
     *    int x, y = the coordinates of the midpoint of moving direction display
     *    int width = the width of the letters in the moving direction display
     *---------------------------------------------------------------
   */
    int height=2*width;
    int spacing=width;
    print_a_text("DIRECTION",x+width/2-2*width,y,width/2,WHITE);
    print_unfilled_window(x,y+spacing,x+2*width+3*spacing,y+spacing+2*height,YELLOW);
}
/********************************************************************************/
void print_move_dir(int dir, int x, int y, int width)
{
    /*--------------------------------------------------------------
     * Updating the moving direction
     * input parameters:
     *    int speed, speed_old = the new and old speed of the motor
     *    bool dir, dir_old = if dir false then left direction otherweise right
     *    int width = the width of the letters in the moving direction display
     *---------------------------------------------------------------
   */
    int height=2*width;
    int spacing=width;

    if (dir == 2)      // printing out R of the order of signals is correct
    {
        print_a_text("L",x+spacing+2*width,y+2*height,width,BLACK);  // deleting old symbols
        print_a_text("R",x+spacing+2*width,y+2*height,width,GREEN);   // printing out "R"
    }
    else if(dir == 0)   // printing out L of the order of signals is correct
    {
        print_a_text("R",x+spacing+2*width,y+2*height,width,BLACK);   // deleting old symbols
        print_a_text("L",x+spacing+2*width,y+2*height,width,RED);         // printing out "L"
    }
    else
    {
        print_a_text("R",x+spacing+2*width,y+2*height,width,BLACK);   // deleting old symbols
        print_a_text("L",x+spacing+2*width,y+2*height,width,BLACK);  // deleting old symbols
    }
}
/********************************************************************************/
inline void print_haw_logo(int x,int y,enum colors color)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    int x, y = the coordinates of the upper left corner of the logo
     *    enum colors color = the color of the logo
     *---------------------------------------------------------------
   */
    print_unfilled_window(x,y,x+50,y+7,color);
    print_window(x+25,y+10,x+75,y+17,color);
    print_unfilled_window(x,y+20,x+50,y+27,color);
    print_window(x+25,y+30,x+75,y+37,color);
    print_unfilled_window(x,y+40,x+50,y+47,color);
    print_window(x+25,y+50,x+75,y+57,color);
    print_a_text("HAW",x+93,y+17,10,color);
    print_a_text("HAMBURG",x+93,y+47,10,color);
}
/********************************************************************************/
inline void print_description(int x,int y, int width, enum colors color)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    int x, y = the coordinates of the upper left corner of the description
     *    int widht = the width of the characters of the text in description
     *    enum colors color = the color of the logo
     *---------------------------------------------------------------
   */
    print_a_text("VEHICLE INFORMATION DISPLAY",x,y,width,color);
    print_a_text("MICROCONTROLLER: TM4C1294NCPDT",x,y+20,width,color);
    print_a_text("AUTHORS: VAHE. TIMO AND SUMAN",x,y+40,width,color);
}
/********************************************************************************/
inline void print_reset(int x,int y, int width)
{
    /*--------------------------------------------------------------
     * input parameters:
     *    int x, y = the coordinates of the upper left corner of the description
     *    int widht = the width of the characters of the text in description
     *    enum colors color = the color of the logo
     *---------------------------------------------------------------
   */
    int height=2*width;
    int spacing=width;
    print_window(x,y+spacing,x+5*width+6*spacing,y+spacing+2*height,RED);
    print_unfilled_window(x,y+spacing,x+5*width+6*spacing,y+spacing+2*height,WHITE);
    print_a_text("RESET",x+2*spacing,y+2*height,width,WHITE);
}
/********************************************************************************/
