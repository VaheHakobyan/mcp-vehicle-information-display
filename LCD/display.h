// Program for LCD Display 800x480 RGB to display a tachometer and trip odometer display
// Display controller Type SSD 1963 => Solomon Systech
// Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
// V0.1 V. Hakobyan 22.11.2020

#ifndef DISPLAY_H_
#define DISPLAY_H_

#include <stdint.h>
#include <stdbool.h>                    // type bool for giop.h
#include "inc/hw_types.h"
#include "inc/tm4c1294ncpdt.h"
#include <stdio.h>                      // Debug only
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>             // GPIO_PIN_X
#include <inc/hw_memmap.h>              // GPIO_PORTX_BASE
#include "sin1.h"
#include <time.h>


// Size of the Display
#define MAX_X 800
#define MAX_Y 480
#define PI 3.14159265359
// constants for display initialization
#define RST 0x10
#define INITIAL_STATE (0x1F)
#define SOFTWARE_RESET (0x01)
#define SET_PLL_MN (0xE2)
#define START_PLL (0xE0)
#define LOCK_PLL (0xE0)                 // same as START_PLL
#define SET_LSHIFT (0xE6)
#define SET_LCD_MODE (0xB0)
#define SET_HORI_PERIOD (0xB4)
#define SET_VERT_PERIOD (0xB6)
#define SET_ADRESS_MODE (0x36)
#define SET_PIXEL_DATA_FORMAT (0xF0)
#define SET_DISPLAY_ON (0x29)
#define SET_DISPLAY_OFF (0x29)          // not tested ??
#define Q15 (1.0/(double)((1<<15)-1))   //int16 Q15  == -32768 to 32767

uint32_t sysClock;                      // Variable for reading out the system clock

// some predefined basic colors to use with names
enum colors
{
    // Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
    BLACK   = 0x00000000,
    WHITE   = 0x00FFFFFF,
    GREY    = 0x00AAAAAA,
    RED     = 0x00FF0000,
    GREEN   = 0x0000FF00,
    BLUE    = 0x000000FF,
    YELLOW  = 0x00FFFF00,
};

// same values as array for indexed colors
// Source: V0.1−V0.4 K.R. Riemschneider - Testprogram two LCD Displays 480x272 and 800x480 RGB
//int colorarray[]={  0x00000000,  //  BLACK
//                    0x00FFFFFF,  //  WHITE
//                    0x00AAAAAA,  //  GREY
//                    0x00FF0000,  //  RED
//                    0x0000FF00,  //  GREEN
//                    0x000000FF,  //  BLUE
//                    0x00FFFF00,  //  YELLOW
//                 };


inline void write_command(unsigned char command);
inline void write_data(unsigned char data);
inline void init_ports_display(void);
void configure_display_controller_large (void);
inline void configPorts(void);
inline void print_window(int min_x,int min_y,int max_x,int max_y, enum colors color);
inline void print_pixel(int x, int y, enum colors color);
inline void print_point(int x, int y, int size, enum colors color);
inline void clear_window(int min_x, int min_y, int max_x, int max_y);
inline void clear_whole_background();
void touch_write(unsigned char value);
unsigned int touch_read();
inline int sgn(int x);
void print_line(double xstart,double ystart,double xend,double yend, enum colors color);
void print_line(double xstart,double ystart,double xend,double yend, enum colors color);
void print_flexible_line(double xstart,double ystart,double xend,double yend, double length, enum colors color);
void print_character(char character, int x, int y, int width, enum colors color);
inline void print_a_text(char *text, int x, int y, int width, enum colors color);
inline void print_unfilled_window(int min_x, int min_y, int max_x, int max_y, enum colors color);
inline void print_ellipse(int xm, int ym, int a, int b, enum colors color);
void print_immutable_components_of_tacho(int x0,int y0, enum colors color);
void print_tacho(double speed,double *speed_old,int x0,int y0);
inline void print_immutable_components_of_odometer( int x, int y, int width, enum colors color);
inline void print_odometer(double distance, double distance_old, int x, int y, int width);
inline void print_immutable_components_of_move_dir(int x, int y, int width);
void print_move_dir(int dir, int x, int y, int width);
inline void print_haw_logo(int x,int y,enum colors color);
inline void print_description(int x,int y, int width, enum colors color);
inline void print_reset(int x,int y, int width);



#endif /* DISPLAY_H_ */
