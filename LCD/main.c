// Program for vehicle information display with using interrupts and systick
// Display controller Type SSD 1963 => Solomon Systech
// Initialisation + Single Pixel + Output
// ARM-Contoller Clock not changed
// V0.1 Timo Kleinhenz and V. Hakobyan

#include "display.h"
#include <stdint.h>
#include <stdbool.h>                    // type bool for giop.h
#include "inc/hw_types.h"
#include "inc/tm4c1294ncpdt.h"
#include <stdio.h>                      // Debug only
#include <string.h>
#include <stdlib.h>
#include <driverlib/sysctl.h>
#include <driverlib/gpio.h>             // GPIO_PIN_X
#include <inc/hw_memmap.h>              // GPIO_PORTX_BASE
#include "driverlib/systick.h" // Tivaware functions: SysTick...
#include "driverlib/interrupt.h"// Tivaware functions: Int...
#include "driverlib/pin_map.h" // Tivaware macros: GPIO_..
// Position of odometer for orientation
#define X 500
#define Y 60
// global variables
volatile int s1Dir = 1;
volatile double distance=0;
volatile double distanceOld=0;
volatile double speed=0;
double speedOld = 0;
volatile bool newSpeed = false;
// Variable for reading out the system clock
uint32_t sysClock;

/********************************************************************************/
void configInterrupts(void)
{
    /*--------------------------------------------------------------
     * Configuration of interrupt pins PP0 amd PP1
     *---------------------------------------------------------------
     */
    int wt=0;
    SYSCTL_RCGCGPIO_R |= 0x2000; // Sys clk for port P
    wt++;
    GPIO_PORTP_DEN_R |= 0x03;    // Digital Enable for PP0 and
    GPIO_PORTP_DIR_R &= ~0x03;   // Define port as Input for PIN PP0 and (Interrupt source)
    GPIO_PORTP_IS_R &= ~0x03;    //detect the edge in pp0
    GPIO_PORTP_IBE_R &= ~0x03;   //GPIO interrupt event (NOT both edges as trigger)
    GPIO_PORTP_RIS_R &= ~0x03;
    GPIO_PORTP_IEV_R |= 0x00;    //rising edge
    GPIO_PORTP_ICR_R |= 0x03;    //reset flag
    GPIO_PORTP_IM_R |= 0x03;     //Interrupt masked (send to interrupt controller)
    GPIO_PORTP_SI_R |= 0x01;     // Specialty of Port P and Q
                                 // here set P0 and P1 as separate source
                                 // <===== Each pin has its own interrupt vector. page 750 and 791 manua l
    //Nested Vektor interrupt init
    NVIC_PRI23_R |=0x00;         //Prio 0 for interrupt in port p Vec Nr.92 Interrupt Nr.76
    //NVIC_EN2_R |= 0x1 << 12;   //enable interrupt 76 for NVIC Use
    NVIC_EN2_R |= 0x3000;        // enable Port P I−Requests in the NVIC
}
/********************************************************************************/
double get_distance()
{
    /*--------------------------------------------------------------
     * Calculating the covered track by the car in km
     * considering the rolling circumference.
     * Circumference = 0.5m -> per pulse = 0.25m (converted in km)
     *---------------------------------------------------------------
    */
    return (double) 0.00025;
}
/****************************************************************/
double get_speed(){
    /*--------------------------------------------------------------
     * Getting the speed of the motor
     * Input parameter:
     *          double distance = the covered track by the car in km
     * Output parameter:
     *          speed in km/h
     * Calculation: HalfOfCircumference/TimeOfOneInterrupt
     *- Circumference = 0.5m -> per interrupt = 0.25m (converted in km)
     *- time = s_taken/3600 (converted in h)
     *---------------------------------------------------------------
     */
    double s_taken=TIMER0_TAV_R*8.3333/1000000000;//get us
    TIMER0_TAV_R=0x0;

    return (double) 0.9/s_taken;
}
/*****************************************************************/
void init_TIMER0(void){
    /**
     * Setup timer 0 for calculating speed
     * */
    int wt=0;
    SYSCTL_RCGCTIMER_R = 0x00000001; wt++; //enable clock timer 0
    TIMER0_CTL_R &= ~0x00000001;//disable timer
    TIMER0_CFG_R =0x00000000;//32 bit mode
    TIMER0_TAMR_R |= 0x02;//periodic mode
    TIMER0_TAMR_R |=0x10;//up counting
    TIMER0_TAILR_R = 4294967296-1;//reload value 1
    TIMER0_ICR_R|=0x1;
    TIMER0_CTL_R |= 0x00000001;//start timer
}
/*****************************************************************/
void systick_handler(void)
{
    /*--------------------------------------------------------------
     * Own systick (System-Clock) interrupt handler
     *---------------------------------------------------------------
     */
    SysTickIntDisable();            // Disable interrupt at systick unit
    SysTickDisable();               // Disable systick unit
    touch_write(0xD0);              // Touch Command for x coordinate
    int x_pos = touch_read();       // read the x coordinate ( 0......4095 )
    touch_write(0x90);              // Touch Command for y coordinate
    int y_pos = touch_read();       // read the y coordinate ( 0.....4095 )
    if ((x_pos>=0) && (x_pos<=700) && (y_pos>= 800) && (y_pos<= 1000))
    {
        distance = 0;               // resetting the distance if RESET button is pushed
    }
    if(speed>=400)                  // limiting the speed to prevent mistakes
        speed = 400;
    // printing all changing components on display
    print_tacho(speed,&speedOld,MAX_X/3-1,MAX_Y/2);
    print_odometer(distance,distanceOld,X,Y,10);
    print_move_dir(s1Dir,X+50,Y+100,10);
    if(!newSpeed)
    {
        speed = 0;                   // if no movement ...
        s1Dir = 1;
    }
    newSpeed = false;
    distanceOld = distance;
    SysTickIntEnable();              // Enable interrupt at systick unit
    SysTickEnable();                 // Enable counting at systick unit}
}
/********************************************************************************/
void init_systick(void)
{
    /*--------------------------------------------------------------
     * Initialization of systick
     * System-Clock-Interrupt every 50ms
     *---------------------------------------------------------------
     */
    IntMasterDisable();
    sysClock = SysCtlClockFreqSet(SYSCTL_OSC_INT |SYSCTL_USE_PLL |SYSCTL_CFG_VCO_480,120000000); // 120 Mhz clock
    SysTickPeriodSet(sysClock/20);       // 50ms Clock interrupt (sysClock = 1s)
    SysTickIntRegister(systick_handler); // Entry in copy of the IVT
    SysTickIntEnable();                  // Switch interrupt source on
    SysTickEnable();
    IntMasterEnable();                   // Switch on
}
/********************************************************************************/
void S1ISR(void){
    /*--------------------------------------------------------------
     * Interrupt handler by S1 (P0)
     *---------------------------------------------------------------
     */
    GPIO_PORTP_ICR_R |= 0x01;            // reset interrupt flag
    s1Dir = (GPIO_PORTP_DATA_R & 0x02);  // reading out the value of s2
}
/********************************************************************************/
void S2ISR(void){
    /*--------------------------------------------------------------
     * Interrupt handler by S2 (P1)
     *---------------------------------------------------------------
     */
    GPIO_PORTP_ICR_R |= 0x02;           // reset interrupt flag
    speed = get_speed();                // updating the speed and distance at every interrupt
    newSpeed = true;
    distance += get_distance();
}
/********************************************************************************/
void main(void)
{
    init_ports_display();                       // Init Port L for Display Control and Port M for Display Data
    configure_display_controller_large();       // Display initialization
    configPorts();                              // Init Port N for User LED and Port P for digital Input from Motor
    clear_whole_background();                   // clearing the display
    // printing all immutable components on Display
    init_TIMER0();
    print_haw_logo(X,300,WHITE);
    print_description(X-80,420,6,WHITE);
    print_immutable_components_of_move_dir(X+50,Y+100,10);
    print_immutable_components_of_tacho(MAX_X/3-1,MAX_Y/2,GREEN);
    print_immutable_components_of_odometer(X,Y,10,GREEN);
    print_reset(X+165,Y,10);
    configInterrupts();                          // Init Port N for User LED, M for touch and Port P for digital Input from Motor
    init_systick();                              // Init System-Clock-Interrupt
    while(1);                                    // busy wating
}
